package ipops

import (
	"errors"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"time"
)

// GetHTML get html content
func GetHTML(url string) (body []byte, err error) {
	// create client and set timeout
	client := &http.Client{
		Timeout: time.Second * 5,
	}
	// Get page
	resp, err := client.Get(url)
	if err != nil {
		return
	}
	if resp.StatusCode != 200 {
		err = errors.New(url + " 请求失败，状态码：" + strconv.Itoa(resp.StatusCode))
		return
	}
	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	return
}

// ParseHTML  parse html to get the info
func ParseHTML(body []byte, complieString string) (content string, err error) {
	re, err := regexp.Compile(complieString)
	if err != nil {
		return "", err
	}
	results := re.FindSubmatch(body)
	if len(results) > 1 {
		content = string(results[1])
		err = nil
		return
	}
	err = errors.New("没有匹配到规则")
	return
}

// GetPublicIPFromIPCN get public ip from https://ip.cn
func GetPublicIPFromIPCN() (publicIP, info string, err error) {
	body, err := GetHTML("https://ip.cn")
	if err != nil {
		return
	}
	publicIP, err = ParseHTML(body, ".*<p>您现在的 IP：<code>(.*)</code></p><p>所.*")
	if err != nil {
		return
	}
	info, err = ParseHTML(body, ".*所在地理位置：<code>(.*)</code></p><p>G.*")
	return
}

// GetPublicIPFromSOHU get public ip from http://pv.sohu.com/cityjson
func GetPublicIPFromSOHU() (publicIP, info string, err error) {
	body, err := GetHTML("http://pv.sohu.com/cityjson")
	if err != nil {
		return
	}
	publicIP, err = ParseHTML(body, ".*\"cip\": \"(.*)\", \"cid.*")
	if err != nil {
		return
	}
	info, err = ParseHTML(body, ".*\"cname\": \"(.*)\"};.*")
	return
}

// GetPublicIP : get public ip from internet
func GetPublicIP() (publicIP, info string, err error) {
	publicIP, info, err = GetPublicIPFromIPCN()
	if err != nil {
		publicIP, info, err = GetPublicIPFromSOHU()
		return
	}
	return
}
